package gooutlate.email;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class Email{
   private String username = "user@host.com";
   private String password = "password";
   public void sendMail(String to,String from) {    
        
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", 587);
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.starttls.enable", true);
        props.put("mail.debug", true);
        props.put("mail.smtp.auth", true);
        Authenticator authenticator = new Authenticator() {
            private PasswordAuthentication pa = new PasswordAuthentication(username, password);
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return pa;
            }
        };
        Session session = Session.getDefaultInstance(props,authenticator);
        session.setDebug(true);

        try {

                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(from));
                InternetAddress[] address = {new InternetAddress(to)};
                message.setRecipients(Message.RecipientType.TO, address);
                message.setSubject("Asunto");
                message.setSentDate(new Date());
                message.setText("Mensaje");
                Transport.send(message);

                System.out.println("Done");

        } catch (MessagingException e) {
                e.printStackTrace();
        }
   }
}
